# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:15.14.0-alpine3.10 AS builder
WORKDIR /app
COPY package*.json /app/
RUN npm install npm@latest -g
COPY ./ /app/
RUN npm run build
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
#FROM nginx
FROM nginx:1.16.0-alpine
COPY --from=builder /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]